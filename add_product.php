<?php  

require_once dirname( __DIR__) .'/JuniorDeveloperTestTask/init.php';

use Product\CrudProduct as Crud;

 $Insert = new Crud(); 
  
 if(isset($_POST["submit"]))  
 {  
    $sku = $_POST['sku'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $size = $_POST['size'];
    $weight = $_POST['weight'];
    $height = $_POST['height'];
    $width = $_POST['width'];
    $length = $_POST['length'];
     
    $Insert->setSku($_POST['sku']);
    $Insert->setName($_POST['name']);
    $Insert->setPrice($_POST['price']);
    $Insert->setWeight($_POST['weight']);
    $Insert->setSize($_POST['size']);
    $Insert->setHeight($_POST['height']);
    $Insert->setWidth($_POST['width']);
    $Insert->setLength($_POST['length']);

    if(empty($sku)){
        $error = "Please, submit required data";
    }else if(empty($name)){
        $error = "Please, submit required data";
    }else if(empty($price)){
        $error = "Please, submit required data";
    }
    else{
      
      $product = $Insert->insert_product();

      if ($product){
      header("location:index.php"); 
      }

    }
 }  
 ?> 
 
 <html lang="en">
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<title>Add Product</title>
</head>
<body>
<br>

<div class="container">
  <div class="panel panel-default">
  <div class="panel-heading"><h2>Product Add</h2></div>
    
    <div class="panel-body">
        <?php 
        if (isset($error)){
        ?>
        <div class ="alert alert-danger col-lg-8">
        <?php echo $error; ?>
        </div>
        <?php
        }
        ?>
        
        <form method="POST" action="">
        
        <button class="btn btn-dark float-right " type="submit" name="submit" style="margin-right:120px; margin-top:-50px;">Save</button>
        <a href="index.php" class="btn btn-dark float-right " style="margin-top:-50px; margin-right:20px;">Cancel</a>
    
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group row">
                <label class="col-sm-2 col-form-label">SKU</label>
                <div class="col-sm-5">
                    <input type="text" name="sku" id="sku" class="form-control" >
                </div>
            </div>
            </div>
            <div class="col-md-12">
                <div class="form-group row">
                <label class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-5">
                    <input type="text" name="name" id="name" class="form-control">
                </div>
            </div>
            </div>
            <div class="col-md-12">
                <div class="form-group row">
                <label class="col-sm-2 col-form-label">Price ($)</label>
                <div class="col-sm-5">
                    <input type="number" name="price" id="price" class="form-control">
                </div>
            </div>
            </div>

            <div class="col-md-12">
            <div class="form-group row">
            <label class="col-sm-2 col-form-label">Type Switcher</label>
            <div class="col-sm-5">
            <select id = "switcher" onchange = "ShowHideDiv()" class="form-control" name="product_type">
                <option>Type Switcher</option>
                <option value="DVD">DVD</option>
                <option value="BOOK">Book</option>
                <option value="FURNITURE">Furniture</option>
              </select>
            </div>
            </div>
            </div>
           
              <br>
              <div class="col-md-12" id="dvd" style="display: none">
              <div class="form-group row">
              <label class="col-sm-2 col-form-label">Size (MB)</label>
              <div class="col-sm-5">
              <input type="text" name="size" id="size" class="form-control" />
              </div>
              </div>
              *Please, provide size*
        </div>

        <br>
              <div class="col-md-12" id="book" style="display: none">
              <div class="form-group row">
              <label class="col-sm-2 col-form-label">Weight (KG)</label>
              <div class="col-sm-5">
              <input type="text" id="size" name="weight" class="form-control" />
              </div>
              </div>
              *Please, provide weight*
        </div>

        <br>
              <div class="col-md-12" id="furtniture" style="display: none">
              <div class="form-group row">
              <label class="col-sm-2 col-form-label">Height (CM)</label>
              <div class="col-sm-5">
              <input type="text" id="size" name="height" class="form-control" />
              </div>
              </div>
              <div class="form-group row">
              <label class="col-sm-2 col-form-label">Width (CM)</label>
              <div class="col-sm-5">
              <input type="text" id="size" name="width" class="form-control" />
              </div>
              </div>
              <div class="form-group row">
              <label class="col-sm-2 col-form-label">Length (CM)</label>
              <div class="col-sm-5">
              <input type="text" id="size" name="length" class="form-control" />
              </div>
              </div>
              *Please, provide dimensions*
        </div>
        </div>   
        </div>
        </form>
        <script type="text/javascript">
            function ShowHideDiv() {
            var switcher = document.getElementById("switcher");
            var dvd = document.getElementById("dvd");
            var book = document.getElementById("book");
            var furtniture = document.getElementById("furtniture");
            dvd.style.display = switcher.value == "DVD" ? "block" : "none";
            book.style.display = switcher.value == "BOOK" ? "block" : "none";
            furtniture.style.display = switcher.value == "FURNITURE" ? "block" : "none";
            }
        </script>
       
  

<footer class="text-muted">
              <hr>
              <div class="container">
                  <p style = "text-align:center; ">
                      Scandiweb Test assignment
                  </p>
                 
              </div>
          </footer>

          
</body>


</html>