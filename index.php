<?php  

require_once dirname( __DIR__) .'/JuniorDeveloperTestTask/init.php';

use Product\CrudProduct as Crud;

$Product = new Crud(); 

 $dvd = $Product->display_product("dvd");
 $book = $Product->display_product("book");
 $furniture = $Product->display_product("furniture");
 
 if($_SERVER['REQUEST_METHOD']=='POST'){

	$dvd_id = $_POST['dvd_id'];
	$dvd_id = implode(",", $dvd_id);
    $Product->delete_product('dvd','dvd_id',$dvd_id);
    $book_id = $_POST['book_id'];
	$book_id = implode(",", $book_id);
    $Product->delete_product('book','book_id',$book_id);
    $fur_id = $_POST['furniture_id'];
	$fur_id = implode(",", $fur_id );
    $Product->delete_product('furniture','furniture_id',$fur_id);

    header("location:index.php");
 }

?>  
 
 
 <!doctype html>
<html lang="en">
 
  <head>
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <title>Product List</title>
  </head>
 
  <body> 
 
          <div class="container">
          <div class="panel panel-default">
          <div class="panel-heading" style="margin-top:30px;">
                  <h2>Product List</h2>
          </div>
          <hr>
          <div class="panel-body">
          <form action="<?php echo $_SERVER['PHP_SELF']; ?>"  method="post">
          <a class="btn btn-dark float-right" style="margin-top:-70px; margin-right:150px;" href="add_product.php">ADD</a>
          <button type="submit" style="margin-top:-70px; " class="btn btn-dark float-right">MASS DELETE</button>

          <?php if (!empty($dvd)){
          foreach ($dvd as $d):?>
              <div class="card" style="width: 13.5rem; float: left; margin: 30px;">
                  <div class="card-body">
                      <input type="checkbox" name="dvd_id[]" value="<?php echo $d['dvd_id']; ?>">
                      <center>
                      <h5 class="card-title"><?php echo $d['sku'];?></h5>
                      <p class="card-text"><?php echo $d['name'];?></p>
                      <p class="card-text"><?php echo number_format($d['price'],2);?> $</p>
                      <p class="card-text">Size: <?php echo $d['size'];?> MB</p>
                      </center>
                  </div>
              </div>
              <?php endforeach; 
              } ?>

          <?php if (!empty($book)){
          foreach ($book as $b):?>
              <div class="card" style="width: 13.5rem; float: left; margin: 30px;">
                  <div class="card-body" >
                      <input type="checkbox" name="book_id[]" value="<?php echo $b['book_id']; ?>">
                      <center>
                      <h5 class="card-title"><?php echo $b['sku'];?></h5>
                      <p class="card-text"><?php echo $b['name'];?></p>
                      <p class="card-text"><?php echo number_format($b['price'],2);?> $</p>
                      <p class="card-text">Weight: <?php echo $b['weight'];?>KG</p>
                      </center>
                  </div>
              </div>
              <?php endforeach; 
              } ?>

 
          <?php if (!empty($furniture)){
          foreach ($furniture as $f):?>
              <div class="card" style="width: 13.5rem; float: left; margin: 30px;">
                  <div class="card-body" >
                      <input type="checkbox" name="furniture_id[]" value="<?php echo $f['furniture_id']; ?>">
                      <center>
                      <h5 class="card-title"><?php echo $f['sku'];?></h5>
                      <p class="card-text"><?php echo $f['name'];?></p>
                      <p class="card-text"><?php echo number_format($f['price'],2);?> $</p>
                      <p class="card-text">Dimension: <?php echo $f['height'];?>x<?php echo $f['width'];?>x<?php echo $f['length'];?></p>
                      </center>
                  </div>
              </div>
              <?php endforeach; 
              } ?>
            
            </form>

          </div>
          
          <footer class="text-muted" style="clear: both; text-align:center;">
              <hr>
              <div class="container">
                  <p>Scandiweb Test assignment</p>
              </div>
          </footer>
 
  </body>
 
</html>