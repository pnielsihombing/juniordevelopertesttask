<?php namespace Product;

abstract class MainProduct
{
    protected $sku;
    protected $name;
    protected $price;
    protected $weight;
    protected $size;
    protected $height;
    protected $width;
    protected $length;
    

    public function __construct(){
        $this->con = mysqli_connect("localhost", "root", "", "products");  
           if(!$this->con)  
           {  
                echo 'Database Connection Error ' . mysqli_connect_error($this->con);  
           }  
    }

    function setSku($sku) { 
        $this->sku = $sku; 
        }

    function getSku() {
        return $this->sku; 
        }

    function setName($name) { 
        $this->name = $name; 
        }

    function getName() { 
        return $this->name; 
        }

    function setPrice($price) { 
        $this->price = $price; 
        }

    function getPrice() { 
        return $this->price; 
        }

    function setWeight($weight){
        $this->weight = $weight;
    }

    function getWeight(){
        return $this->weight;
    }

    function setSize($size){
        $this->size = $size;
    }

    function getSize(){
         return $this->size;
     }

     function setHeight($height)
     {
         $this->height = $height;
     }

     function getHeight()
     {
         return $this->height;
     }

     function setWidth($width){
         $this->width = $width;
     }

     function getWidth(){
          return $this->width;
     }

     function setLength($length){
         $this->length = $length;
     }

     function getLength(){
         return $this->length;
     }


     abstract public function insert_product();
    
     abstract public function display_product($table);

     abstract public function delete_product($table,$id,$data);

}

?>

